// imports the Course model

const Course = require("../models/Course");

// const User = require("../models/User");


// import bcrypt - for encrypting password

const bcrypt = require("bcrypt");


// import jwt - user authentication

const auth = require("../auth");


// Controller function for creating a course

/*module.exports.addCourse = (reqBody) => {

	// Create a variable "newCourse" and instantiates a new "Course" object

	let newCourse = new Course({
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error){
			return false;
		}
		else{
			return true;
		}
	})


}*/


// s33 Activity
// WDC028-34 Express.js - API Development (Part 3)

// 2. Refactor the addCourse controller method to implement admin authentication for creating a course.

// Controller function for creating a course
module.exports.addCourse = (data) => {

	// User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return false;
	};
	

};






// Controller function for getting all courses 

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}



// Controller function for retrieving a specific course

module.exports.getCourse = (reqParams) => {

	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}


// Controller function for updating a course

module.exports.updateCourse = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated

	let updatedCourse = {

		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {

			if(error){
				return false;
			}
			else{
				return "a";
			}
	})
}



// s35 Activity

// 2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.

module.exports.archiveCourse = (reqParams, data) => {

    if(data.isAdmin) {

        let archivedCourse = {

            isActive : data.course.isActive
        }

        return Course.findByIdAndUpdate(reqParams.courseId, archivedCourse).then((course, error) => {
            
            if(error){
                return false;
            }
            else{
                return true;
            }
        })
    }
    // Non-admin
    else{
        return Promise.resolve(false);
    }
}


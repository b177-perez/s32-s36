// s32 Activity

/*
1. Create a User model with the following properties:
  - firstName - String
  - lastName - String
  - email -String
  - password - String
  - isAdmin - Boolean
  - mobileNo - String
  - enrollments - Array of objects
    - courseId - String
    - enrolledOn - Date (Default value - new Date object)
    - status - String (Default value - Enrolled)
*/


const mongoose = require("mongoose");


const userSchema = new mongoose.Schema({

	firstName : {
		type : String,
		required : [true, "FirstName is required"]
	},
	lastName : {
		type : String,
		required : [true, "LastName is required"]
	},
	email : {
		type : String,
		required : [true, "Email is required"]
	},
	password : {
		type : String,
		required : [true, "Password is required"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	mobileNo : {
		type : String,
		required : [true, "Mobile Number is required"]
	},
	enrollments : [
		{
			courseId : {
				type : String,
				required: [true, "CourseId is required"]
			},
			enrolledOn : {
				type : Date,
				default : new Date()
			},
			status : {
				type : String,
				default : "Enrolled"
			}
		}
	]

})


module.exports = mongoose.model("User", userSchema);


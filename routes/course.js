const express = require("express");

const router = express.Router();

const courseController = require("../controllers/course");


// const userController = require("../controllers/user");
const auth = require("../auth");


// Route for creating a course

/*router.post("/", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
})*/



// s33 Activity
// WDC028-34 Express.js - API Development (Part 3)

// 1. Refactor the course route to implement user authentication for the admin when creating a course.


// Route for creating a course
router.post("/", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data.isAdmin);

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
})




// Route for retrieving all the courses

router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
})



// Route for retrieving specific course

router.get("/:courseId", (req, res) => {

	console.log(req.params.courseId);

	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
})


// Route for updating a course

router.put("/:courseId", auth.verify, (req, res) => {

	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));

})



// s35 Activity

// 1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the url.

router.put("/:courseId/archive", auth.verify, (req, res) =>{

    const archive = {

        course : req.body,
        isAdmin : auth.decode(req.headers.authorization).isAdmin
    }

    courseController.archiveCourse(req.params, archive).then(resultFromController => res.send(resultFromController))
})


module.exports = router;
